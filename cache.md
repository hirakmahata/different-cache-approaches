# DIFFRENT CACHING APPROACHES


Caching is a technique to speed up data lookups (data reading). Instead of reading data directly from its source we can use caching. Data will be fetched to read directly from the cache. 


## MAIN PRINCIPAL OF CACHING
Caching is totally depended on cache.
A cache is a storage area that is closer to the entity needing it than the original source. Accessing this cache is typically faster than accessing the data from its original source.  It will increase our efficiency of accessing dada. Cache is basically stored in your computer or on disk. Memory cache is typically faster to read than disk cache. But memory cache does not survive if your system restarts.

### TYPES OF CACHING
Whenever we browse any browser they access our cache to provide efficient browsing. Probalby everybody had heard the term "clear your cache". people maximum time refer to the browser caching. But there are some more types of caching approachs are there.

1. Web Caching (Browser/Proxy/Gateway)
2. Data Caching
3. Application/Output Caching
4. Distributed Caching

These are the 4 types of caching we use most of the times.
 
  #### Web Caching(Browser/Proxy/Gateway)
  It includes those 3 types of caching: Browser chaching, Proxy chachig & Gateway caching. Browser, Proxy, and Gateway caching work differently but have the same goal: to reduce overall network traffic and latency. Browser caching works at the individual user level but later two part of caching works on much larger level. The latter two allow for cached information to be shared across larger groups of users. Commonly cached data could be DNS (Domain Name Server) data, used to resolve domain names to the IP addresses and mail server records. broser cache helps users to navigate the website which they have already visited. This advantages is taken by most hosted companies & a lot of developers now a days.

  #### Data Caching
  Data chaching is the most popular and useful approaches. Whenever we browse the data, Data is fetched fromthe database. The cache stores the data which is being retrived most frequently. It helps the browsing by providing the cache's data rather than fetching databade every times. The database is the bottle neck for almost all web application,so the fewer DB calls the better. Most DB solutions will also make an attempt to cache frequently used queries in order to reduce turnaround time. For example, MS SQL uses Execution Plans for Store Procedures and Queries to speed up the process time.

  #### Application/Output Caching
  Every content management system has their own cache mechanism. But sometimes we ignore them. It is not the better way. Caching is invented to provide you better browsing. So, implement it whenever it needed. Catching can drastically reduse our browsing time & loading the page. Different than Data Caching, which stores raw data sets, Application/Output Caching often utilizes server level caching techniques that cache raw HTML. It can be per page of data, parts of a page (headers/footers) or module data, but it is usually HTML markup. If you use this technique you can reduce your browsing time to a large amount of percentage.


  #### Distributed Caching
  The most Big companies like google, amazon, youtube, use those technique of Distributed Caching.
 This approach allows the web servers to pull and store from distributed server’s memory. Once implemented, it allow the web server to simply serve pages and not have to worry about running out of memory. This also allows the distributed cache to meade up of cluster & cheaper machine to serve the memory only. you can add new machine of memory at any time without disrupting your users. sometimes we wonder that big companies like google, youtube, they have so quick response even if they have thousands & thousands requests. here the answers lies. they use distributed caching.  They use Clustered Distributed Caching along with other techniques to infinitely store the data in memory because memory retrieval is faster than file or DB retrieval. it  is very good to implement those things for every  large companies to handle simultaneous requests.
 




**except those catching there are also many king of caching happens when we run our system on a daily basis**

Those are :
#### Page Output Caching
This catching is used when we fetch information or data at the page level. The output cache directive can be use at the top of the **.aspx page** in output catching.

> < %@OutputCache Duration= "60" VaryByParam= "DepartmentId"% >

#### CPU caching
Whenever we need any data in your computer CPU, it goes to the catche memory rather than searching in local disk. this process is called CPU caching. It helps our computer  to word fast without waiting for data. It reduces data fetching time.



**references**

<https://www.ironistic.com/four-major-caching-types-and-their-differences/>

<http://net-informations.com/faq/asp/cachtype.htm>

<https://en.wikipedia.org/wiki/Cache_(computing)>
